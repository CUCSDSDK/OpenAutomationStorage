package com.cloupia.feature.storage.accounts.handler;

import org.apache.log4j.Logger;

import com.cisco.cuic.api.client.JSON;
import com.cloupia.feature.storage.accounts.StorageAccount;
import com.cloupia.feature.storage.accounts.api.StorageAccountAPI;
import com.cloupia.lib.connector.account.AbstractInfraAccount;
import com.cloupia.lib.connector.account.AccountUtil;
import com.cloupia.lib.connector.account.PhysicalConnectivityStatus;
import com.cloupia.lib.connector.account.PhysicalConnectivityTestHandler;
import com.cloupia.lib.connector.account.PhysicalInfraAccount;
import com.cloupia.model.cIM.InfraAccountTypes;

/**
 * This class is used to test the connection of the real device reachable from the UCSD.
 * 
 * 
 *
 */
public class StorageAccountConnectionHandler extends PhysicalConnectivityTestHandler{
	static Logger logger = Logger.getLogger(StorageAccountConnectionHandler.class);
	
	@Override
	public PhysicalConnectivityStatus testConnection(String accountName)
			throws Exception {
		
		StorageAccount acc = getFooCredential(accountName);
		PhysicalInfraAccount infraAccount = AccountUtil.getAccountByName(accountName);
		PhysicalConnectivityStatus status = new PhysicalConnectivityStatus(infraAccount);
		status.setConnectionOK(false);
		if(infraAccount != null)
		{
			if(infraAccount.getAccountType() != null)
			{
				logger.info(infraAccount.getAccountType());
				
				
				
				
				//Instead of Foo Account , real account type needs to specified.
				if(infraAccount.getAccountType().equals("Foo Account"))
				{
					logger.info("Inside if condition");
					/**
					 * The below snippet will be used for the real device connection.
					 * 
					 */
					/*try
					{
						StorageAccountAPI.getFooAccountAPI(acc);	
						logger.info("*******Setting test Connection as true*****");
					}
					catch(Exception e)
					{
						status.setConnectionOK(false);
						status.setErrorMsg("Failed to establish connection with the Device.");
						logger.debug("Test Connection is failed");
						return status;
					}*/
					
					status.setConnectionOK(true);
					logger.debug("Connection is verified");
					
				}
			}
			
			
		}
		logger.info("Returning status "+status.isConnectionOK());
		return status;
	}
	
	private static StorageAccount getFooCredential(String accountName) throws Exception{
		PhysicalInfraAccount acc = AccountUtil.getAccountByName(accountName);
		String json = acc.getCredential();
		AbstractInfraAccount specificAcc  =  (AbstractInfraAccount) JSON.jsonToJavaObject(json, StorageAccount.class);
		specificAcc.setAccount(acc);
		
		return (StorageAccount) specificAcc;
		
	}
}
