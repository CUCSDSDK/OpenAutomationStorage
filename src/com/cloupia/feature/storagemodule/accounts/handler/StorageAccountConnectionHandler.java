package com.cloupia.feature.storagemodule.accounts.handler;

import org.apache.log4j.Logger;

import com.cisco.cuic.api.client.JSON;
import com.cloupia.feature.storagemodule.accounts.StorageInfraAccount;
import com.cloupia.feature.storagemodule.constants.StorageModuleConstants;
import com.cloupia.lib.connector.account.AbstractInfraAccount;
import com.cloupia.lib.connector.account.AccountUtil;
import com.cloupia.lib.connector.account.PhysicalConnectivityStatus;
import com.cloupia.lib.connector.account.PhysicalConnectivityTestHandler;
import com.cloupia.lib.connector.account.PhysicalInfraAccount;

/**
 * This class is used to test the connection of the real device reachable from
 * the UCSD.
 * 
 * 
 *
 */
public class StorageAccountConnectionHandler extends PhysicalConnectivityTestHandler {
	static Logger logger = Logger.getLogger(StorageAccountConnectionHandler.class);

	@Override
	public PhysicalConnectivityStatus testConnection(String accountName) throws Exception {

		StorageInfraAccount acc = getAccountCredential(accountName);
		PhysicalInfraAccount infraAccount = AccountUtil.getAccountByName(accountName);
		PhysicalConnectivityStatus status = new PhysicalConnectivityStatus(infraAccount);
		status.setConnectionOK(false);
		if (infraAccount != null) {
			if (infraAccount.getAccountType() != null) {
				logger.info(infraAccount.getAccountType());

				// Instead of Foo Account , real account type needs to
				// specified.
				if (infraAccount.getAccountType().equals(StorageModuleConstants.INFRA_ACCOUNT_TYPE)) {
					logger.info("Inside if condition");
					/**
					 * The below snippet will be used for the real device
					 * connection.
					 * 
					 */
					/*
					 * try { StorageAccountAPI.getFooAccountAPI(acc);
					 * logger.info("*******Setting test Connection as true*****"
					 * ); } catch(Exception e) { status.setConnectionOK(false);
					 * status.
					 * setErrorMsg("Failed to establish connection with the Device."
					 * ); logger.debug("Test Connection is failed"); return
					 * status; }
					 */

					status.setConnectionOK(true);
					logger.debug("Connection is verified");

				}
			}

		}
		logger.info("Returning status " + status.isConnectionOK());
		return status;
	}

	private static StorageInfraAccount getAccountCredential(String accountName) throws Exception {
		PhysicalInfraAccount acc = AccountUtil.getAccountByName(accountName);
		String json = acc.getCredential();
		AbstractInfraAccount specificAcc = (AbstractInfraAccount) JSON.jsonToJavaObject(json,
				StorageInfraAccount.class);
		specificAcc.setAccount(acc);

		return (StorageInfraAccount) specificAcc;

	}
}
