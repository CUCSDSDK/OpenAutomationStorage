package com.cloupia.feature.storagemodule.accounts.inventory;

import org.apache.log4j.Logger;
import org.apache.log4j.Logger;

import com.cloupia.feature.storagemodule.accounts.StorageAccount;
import com.cloupia.fw.objstore.ObjStore;
import com.cloupia.fw.objstore.ObjStoreHelper;
import com.cloupia.service.cIM.inframgr.collector.controller.PersistenceListener;
import com.cloupia.service.cIM.inframgr.collector.model.ItemResponse;

/**
 * This is listener for persisting the inventory.
 * 
 */
public class StorageAccountCollectorInventroyPersistenceListener extends PersistenceListener {
	static Logger logger = Logger.getLogger(StorageAccountCollectorInventroyPersistenceListener.class);

	@Override
	public void persistItem(ItemResponse arg0) throws Exception {
		logger.info(":: persist Item ::");
		/**
		 * This is for dummy implementation. For real implementation you can
		 * persist according to your requirement.
		 */
		ObjStore<StorageAccount> store = ObjStoreHelper.getStore(StorageAccount.class);

		StorageAccount obj = new StorageAccount();
		obj.setAccountName("Foo-Test-1");
		obj.setIp("182.28.23.34");
		obj.setStatus("Active");
		store.insert(obj);

		StorageAccount obj2 = new StorageAccount();
		obj2.setAccountName("Foo-Test-1");
		obj2.setIp("182.28.23.34");
		obj2.setStatus("Active");
		store.insert(obj2);

	}

}
