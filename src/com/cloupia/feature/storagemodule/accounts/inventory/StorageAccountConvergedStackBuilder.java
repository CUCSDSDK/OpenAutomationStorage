package com.cloupia.feature.storagemodule.accounts.inventory;

import java.util.ArrayList;
import java.util.List;

import com.cloupia.feature.storagemodule.constants.StorageModuleConstants;
import com.cloupia.model.cIM.ConvergedStackComponentDetail;
import com.cloupia.model.cIM.ReportContextRegistry;
import com.cloupia.service.cIM.inframgr.reports.contextresolve.ConvergedStackComponentBuilderIf;

public class StorageAccountConvergedStackBuilder implements ConvergedStackComponentBuilderIf {

	@Override
	public ConvergedStackComponentDetail buildConvergedStackComponent(String arg0) throws Exception {
		ConvergedStackComponentDetail detail = new ConvergedStackComponentDetail();
		detail.setModel("StorageModule Model");
		detail.setOsVersion("1.0");
		detail.setVendorLogoUrl("/app/uploads/openauto/storagemodule_logo.png");
		detail.setIconUrl("/app/uploads/openauto/storagemodule_logo.png");
		detail.setMgmtIPAddr("172.29.109.219");
		detail.setStatus("OK");
		detail.setVendorName("StorageModule");
		detail.setContextType(ReportContextRegistry.getInstance()
				.getContextByName(StorageModuleConstants.INFRA_ACCOUNT_TYPE).getType());
		detail.setLayerType(3);
		detail.setComponentSummaryList(getSummaryReports());
		return detail;
	}

	private List<String> getSummaryReports() throws Exception {

		List<String> rpSummaryList = new ArrayList<String>();
		rpSummaryList.add("test");
		rpSummaryList.add("test2");
		return rpSummaryList;

	}
}
