package com.cloupia.feature.storagemodule.constants;


public class StorageModuleConstants {

	public static final String DEVICE_LIST_PROVIDER = "Storage_DeviceListProvider";
	public static final String NEXUS_DEVICE_TABLE = "Storage_DeviceTable";
	
	public static final String TEMP_EMAIL_ADDRESSES = "Storage_email_address_list";
    public static final String NEXUS_DEVICE_LIST = "Storage_nexus_device_list";
   
    public static final String EMAIL_TASK_OUTPUT_NAME = "Datacenter Email Addresses";
    public static final String EMAIL_TASK_OUTPUT_TYPE = "e-mail-as-string";
    
	public static final String STORAGEMODULE_NEXUS_DEVICES_LOV_PROVIDER = "NetworkDeviceList";

	public static final String STORAGEMODULE_HELLO_WORLD_NAME = "Storage_name_from_other_tasks";
	
	//this is the unique integer i'm giving for my Storage  collector, it's a good idea
	//to use some large number past 1000 so you avoid any potential collisions
	public static final int STORAGEMODULE_ACCOUNT_TYPE = 6000;
	
	//some Storage  strings used to represent inventory items
	public static final String INTERFACES = "interfaces";
	public static final String PORTS = "ports";
	
	public static final String STORAGEMODULE_INVENTORY_COLLECTOR_NAME = "Storage _Inventory_Collector";
	
	public static final String STORAGEMODULE_VLAN_RESOURCE_TYPE = "StorageModule.vlan.per.group.usage";
	public static final String STORAGEMODULE_VLAN_RESOURCE_DESC = "Max Storage  VLANs per group";
	
	public static final int STORAGE_MENU_1 = 90004;
	
	public static final String STORAGEMODULE_CONTEXT_ONE = "StorageModule.Storage .context.one";
	public static final String STORAGEMODULE_CONTEXT_ONE_LABEL = "Storage Context One";
	
	public static final String INFRA_ACCOUNT_LABEL = "Storage Account";
	public static final String INFRA_ACCOUNT_TYPE = "Storage Account";
	
	public static final String STORAGE_ACCOUNT_DRILLDOWN_NAME = "StorageModule.account.sample.child.drilldown.report";
	public static final String STORAGE_ACCOUNT_DRILLDOWN_LABEL = "Storage  Account Drilldown Sample";

}
